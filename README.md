ZF2 + docker lamp.
Requires docker and docker-compose.

**RUN**:
docker-compose up -d

**STOP**:
docker-compose stop

**CONTAINERS LIST**:
docker-compose ps

To run composer and symfony commands use php container.
NOTE: use www-data user instead of default (root)

Run single command:
docker exec -u www-data -it CONTAINER_NAME COMMAND

Connect to terminal:
docker exec -u www-data -it CONTAINER_NAME /bin/bash


**DEPLOY**:  
0) Rename doctrine.local.php.dist to doctrine.local.php and change database credentials if needed.  
(optional) Rename zenddevelopertools.local.php.dist to zenddevelopertools.local.php to enable profiler.


1) Optional. Run docker containers, connect to php container terminal  
docker-compose up -d  
docker exec -u www-data -it zf2task_php_1 /bin/bash  
NOTE: zf2task_php_1 is default php container name, but yours can be different. Use "docker-compose ps" to get correct container name.  

2) set up project (ensure all containers are running if using docker)  
If you skipped steep 1, run this commands from "project" directory.   
  
composer install  
./vendor/bin/doctrine-module orm:schema-tool:update --dump-sql --force  
./vendor/bin/doctrine-module dbal:import /var/www/zf2/testData.sql

**INFO**  
1) URL: http://0.0.0.0:800/ or http://localhost:800/  
2) Stock API: /api/v1/stock/update-stock  
Accepts only POST. Request should have "Content-Type: application/json" header.  
Example request body: {"content":{"carId":"car3","status":"sold"}}  
3) DB available at 0.0.0.0:33060 root@root
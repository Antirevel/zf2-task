#

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

# Data

INSERT INTO `car_stock`.`color` (`name`) VALUES ('Red');
INSERT INTO `car_stock`.`color` (`name`) VALUES ('Green');
INSERT INTO `car_stock`.`color` (`name`) VALUES ('Blue');

INSERT INTO `car_stock`.`car_vendor` (`name`) VALUES ('Audi');
INSERT INTO `car_stock`.`car_vendor` (`name`) VALUES ('BMW');
INSERT INTO `car_stock`.`car_vendor` (`name`) VALUES ('Ford');
INSERT INTO `car_stock`.`car_vendor` (`name`) VALUES ('Honda');

INSERT INTO `car_stock`.`car_model` (`name`, `vendor_id`) VALUES ('Model 1', '1');
INSERT INTO `car_stock`.`car_model` (`name`, `vendor_id`) VALUES ('Model 2', '2');
INSERT INTO `car_stock`.`car_model` (`name`, `vendor_id`) VALUES ('Model 3', '3');
INSERT INTO `car_stock`.`car_model` (`name`, `vendor_id`) VALUES ('Model 4', '4');

INSERT INTO `car_stock`.`car` (`enginePower`, `photo`, `price`, `carModel_id`, `color_id`, `inStock`, `status`, `externalId`) VALUES ('100', '1.jpg', '1000', '1', '1', '1', 'available', 'car1');
INSERT INTO `car_stock`.`car` (`enginePower`, `photo`, `price`, `carModel_id`, `color_id`, `inStock`, `status`, `externalId`) VALUES ('200', '2.jpg', '2000', '2', '2', '1', 'available', 'car2');
INSERT INTO `car_stock`.`car` (`enginePower`, `photo`, `price`, `carModel_id`, `color_id`, `inStock`, `status`, `externalId`) VALUES ('300', '3.jpg', '3000', '3', '3', '1', 'available', 'car3');
INSERT INTO `car_stock`.`car` (`enginePower`, `photo`, `price`, `carModel_id`, `color_id`, `inStock`, `status`, `externalId`) VALUES ('400', '4.jpg', '4000', '4', '1', '1', 'available', 'car4');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

<?php


namespace CarStock\Controller;


use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class CarStockController
 * @package CarStock\Controller
 */
class CarStockController extends AbstractActionController
{

    /**
     * Stock list.
     *
     * @return ViewModel
     */
    public function indexAction()
    {

        /** @var EntityManager $entityManager */
        $entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $cars = $entityManager->getRepository('CarStock\Entity\Car')->findBy([
            'inStock' => true
        ]);

        $view = new ViewModel([
            'cars' => $cars
        ]);

        return $view;

    }

}
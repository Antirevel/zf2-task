<?php


namespace CarStock\Controller;


use CarStock\Entity\Car;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Class StockApiController
 * @package CarStock\Controller
 */
class StockApiController extends AbstractRestfulController
{

    const METHOD_POST = 'POST';

    /**
     * Car status update.
     *
     * @return JsonModel
     */
    # TODO: auth, method restriction
    public function updateStockAction()
    {

        if (!$this->request->isPost()) {
            return $this->badRequest(405, 'Bad request method');
        }


        $data = $this->processBodyContent($this->request);

        if (!$data['content'] || !$data['content']['carId'] || !$data['content']['status']) { # TODO: use framework validation?
            return $this->badRequest(405, 'Bad request data');
        }

        /** @var EntityManager $entityManager */
        $entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        /** @var Car $car */
        $car = $entityManager->getRepository('CarStock\Entity\Car')->findOneBy([
            'externalId' => $data['content']['carId']
        ]);

        if (!$car) {
            return $this->badRequest();
        }


        try {
            $car->setStatus($data['content']['status']);
            $entityManager->flush($car);
        } catch (\Exception $e) {
            return $this->badRequest();
        }

        return $this->success();

    }


    /**
     * @param array $data
     * @return JsonModel
     */
    private function jsonModel(array $data)
    {
        return new JsonModel($data);

    }


    /**
     * @param int $code
     * @param string $message
     * @return JsonModel
     */
    private function badRequest($code = 400, $message = '')
    {
        $this->response->setStatusCode($code);
        return $this->jsonModel(['success' => false, 'message' => $message]);
    }


    /**
     * @return JsonModel
     */
    private function success()
    {
        $this->response->setStatusCode(200);
        return $this->jsonModel(['success' => true]);
    }


}
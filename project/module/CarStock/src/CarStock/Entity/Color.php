<?php


namespace CarStock\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="color")
 * @ORM\Entity
 */
class Color
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @var
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;


    /**
     * @var
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sample;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Color
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Color
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSample()
    {
        return $this->sample;
    }

    /**
     * @param mixed $sample
     * @return Color
     */
    public function setSample($sample)
    {
        $this->sample = $sample;
        return $this;
    }




}
<?php


namespace CarStock\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="car_vendor")
 * @ORM\Entity
 */
class CarVendor
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var
     *
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return CarVendor
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }




}
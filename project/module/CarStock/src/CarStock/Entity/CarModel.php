<?php


namespace CarStock\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="car_model")
 * @ORM\Entity
 */
class CarModel
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;


    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="CarStock\Entity\CarVendor")
     */
    protected $vendor;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CarModel
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param mixed $vendor
     * @return CarModel
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
        return $this;
    }







}
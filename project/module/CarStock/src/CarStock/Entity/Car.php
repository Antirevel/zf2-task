<?php


namespace CarStock\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="car")
 * @ORM\Entity
 */
class Car
{

    const STATUS_AVAILABLE = 'available';
    const STATUS_SOLD = 'sold';
    const STATUS_BOOKED = 'booked';

    const IMAGE_BASE_PATH = '/img/car/';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $equipment;

    /**
     * @var
     *
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $enginePower;


    /**
     * @var
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $photo;


    /**
     * @var
     *
     * @ORM\Column(type="decimal", precision=11, scale=2, nullable=true)
     */
    protected $price;


    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="CarStock\Entity\CarModel")
     */
    protected $carModel;


    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="CarStock\Entity\Color")
     */
    protected $color;


    /**
     * @var
     *
     * @ORM\Column(type="boolean")
     */
    protected $inStock = true;


    /**
     * @var
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $status;


    /**
     * @var
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $externalId;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * @param mixed $equipment
     * @return CarStock
     */
    public function setEquipment($equipment)
    {
        $this->equipment = $equipment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnginePower()
    {
        return $this->enginePower;
    }

    /**
     * @param mixed $enginePower
     * @return CarStock
     */
    public function setEnginePower($enginePower)
    {
        $this->enginePower = $enginePower;
        return $this;
    }

    /**
     * @param bool $raw
     * @return mixed
     */
    public function getPhoto($raw = false)
    {

        if ($raw) {
            return $this->photo;
        }

        return self::IMAGE_BASE_PATH . $this->photo;
    }

    /**
     * @param mixed $photo
     * @return CarStock
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return CarStock
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarModel()
    {
        return $this->carModel;
    }

    /**
     * @param mixed $carModel
     * @return CarStock
     */
    public function setCarModel($carModel)
    {
        $this->carModel = $carModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     * @return CarStock
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return CarStock
     */
    public function setStatus($status)
    {

        if (!in_array($status, [self::STATUS_AVAILABLE, self::STATUS_BOOKED, self::STATUS_SOLD])) {
            throw new \LogicException('Wrong stock status.');
        }

        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * @param mixed $inStock
     * @return Car
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param mixed $externalId
     * @return Car
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }



}
<?php
return [
    'controllers' => [
        'invokables' => [
            'CarStock\Controller\CarStock' => 'CarStock\Controller\CarStockController',
            'CarStock\Controller\StockApi' => 'CarStock\Controller\StockApiController',
        ],
    ],

    'router' => [
        'routes' => [
            'carStock' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'CarStock\Controller\CarStock',
                        'action'     => 'index',
                    ],
                ],
            ],
            'stockApi' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/api/v1/stock[/:action]',
                    'defaults' => [
                        'controller' => 'CarStock\Controller\StockApi',
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => [
            'car_stock' => __DIR__ . '/../view',
        ],
    ],

    'doctrine' => array(
        'driver' => array(
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'my_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/CarStock/Entity',
                ),
            ),

            // default metadata driver, aggregates all other drivers into a single one.
            // Override `orm_default` only if you know what you're doing
            'orm_default' => array(
                'drivers' => array(
                    // register `my_annotation_driver` for any entity under namespace `My\Namespace`
                    'CarStock\Entity' => 'my_annotation_driver'
                )
            )
        )
    )

];